node default {

  # Nginx
  notify { 'before': }
  -> class { 'nginx': 
    worker_processes   => 2,
    worker_connections => 2048
  }
  -> notify { 'last': }

 notify { 'enduser-before': }
 notify { 'enduser-after': }

 include nginxphp

 # Ntp
 class {'ntp':
   require => Notify['enduser-before'],
   before  => Notify['enduser-after'],
 }
}
